﻿using ASP.NETCoreWebApplication.Repository;
using Microsoft.AspNetCore.Mvc;

namespace ASP.NETCoreWebApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CounterController : Controller
    {
        private ICounterRepository _counterRepository;

        public CounterController(ICounterRepository counterRepository)
        {
            this._counterRepository = counterRepository;
        }

        [HttpGet]
        public int Get()
        {
            _counterRepository.incrementCounter();
            return _counterRepository.GetCounter();
            
        }
    }
}