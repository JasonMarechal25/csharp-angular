﻿using System;

namespace ASP.NETCoreWebApplication.Repository
{
    public interface ICounterRepository : IDisposable
    {
        int GetCounter();
        void incrementCounter();
    }
}