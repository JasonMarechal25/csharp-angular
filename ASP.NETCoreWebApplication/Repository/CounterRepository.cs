﻿using System;

namespace ASP.NETCoreWebApplication.Repository
{
    public class CounterRepository : ICounterRepository, IDisposable
    {
        private bool disposed = false;
        public int counter = 0;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    counter = 0;
                }
            }
            this.disposed = true;
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int GetCounter()
        {
            return counter;
        }

        public void incrementCounter()
        {
            counter++;
        }
    }
}